<?php

/*******************************************************************************
            ____                      __  ____ ____
           / __ \___  _________ ___  /  |/  (_) / /
          / /_/ / _ \/ ___/ __ `__ \/ /|_/ / / / / 
         / ____/  __/ /  / / / / / / /  / / / / /  
        /_/    \___/_/  /_/ /_/ /_/_/  /_/_/_/_/

  Provides your fine-grained Drupal permissions
  Author : Samuele Santi <s.santi AT squadrainformatica.com>
  Copyright (c) 2010 2V S.r.l.

*******************************************************************************/

define('PERMMILL_TYPE_REQUIRED', 'required');
define('PERMMILL_TYPE_SUFFICIENT', 'sufficient');

define('PERMMILL_MATCH_EQUAL', 0);
define('PERMMILL_MATCH_BEGINS', 1);
define('PERMMILL_MATCH_REGEXP', 2);

/**
 * Implementation of hook_menu()
 * @return unknown_type
 */
function permmill_menu() {
  $items = array();
  $items['admin/build/permmill'] = array(
    'title' => 'PermMill',
    'description' => 'Configure fine-grained permissions to pages on the site.',
    'page callback' => 'permmill_admin_settings_overview',
    'access arguments' => array('administer permmill'),
    'file' => 'permmill.admin.inc',
  );
  
  $items['admin/build/permmill/menu_access'] = array(
    'title' => 'Menu Access',
    'description' => 'Add permissions to pages',
    'page callback' => 'permmill_admin_menu_access_list',
    'access arguments' => array('administer permmill'),
    'file' => 'permmill.admin.inc',
  );
  $items['admin/build/permmill/menu_access/list'] = array(
    'title' => 'List',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  $items['admin/build/permmill/menu_access/add'] = array(
    'title' => 'Add',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('permmill_admin_menu_access_form'),
    'access arguments' => array('administer permmill'),
    'file' => 'permmill.admin.inc',
  );
  $items['admin/build/permmill/menu_access/%/edit'] = array(
    'title' => 'Edit page rule',
    'type' => MENU_CALLBACK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('permmill_admin_menu_access_form', 3),
    'access arguments' => array('administer permmill'),
    'file' => 'permmill.admin.inc',
  );
  return $items;
}

/**
 * Implementation of hook_menu_alter()
 * 
 * Looks for pages matching rules.
 * If a page matches
 *    - change ['access callback'] to permmill_user_access_custom
 *    - set ['access arguments'][0] to array('required' => array(), 'sufficient' => array())
 * 
 * @param &$items
 * @return void
 * @see http://api.drupal.org/api/function/hook_menu_alter/6
 */
function permmill_menu_alter(&$items) {
  $rules = _permmill_get_all_rules();
  foreach ($items as $key => $item) {
    foreach ($rules as $rule) {
      switch ($rule->match_type) {
        case PERMMILL_MATCH_EQUAL:
          $rule_matching = ($key == $rule->page_match);
          break;
        case PERMMILL_MATCH_BEGINS:
          $rule_matching = (strpos($key, $rule->page_match) === 0);
          break;
        case PERMMILL_MATCH_REGEXP:
          $rule_matching = preg_match($rule->page_match, $key);
          break;
        default:
          $rule_matching = FALSE;
      }
      if ($rule_matching) {
        if ($items[$key]['access callback'] != "permmill_user_access_custom") {
          // First time setting this item as matching
          $items[$key]['access callback'] = "permmill_user_access_custom";
          $items[$key]['access arguments'] = array(
            array(
              'required' => array(),
              'sufficient' => array($items[$key]['access arguments'][0]),
            ),
          );
        }
        $items[$key]['access arguments'][0][$rule->perm_type][] = $rule->permission;
      }
    }
  }
}


/**
 * Implementation of hook_perm()
 * @return array
 */
function permmill_perm() {
  $perms = array('administer permmill');
  $rules = _permmill_get_all_rules();
  foreach ($rules as $rule) {
    if (!in_array($rule->permission, $perms)) {
      $perms[] = $rule->permission;
    }
  }
  return $perms;
}

/**
 * Custom 'access callback' function to process multiple permissions
 * @param $perms
 * @return bool
 */
function permmill_user_access_atleastone($perms) {
  foreach ($perms as $perm) {
    if (user_access($perm)) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Custom user_access-like function
 * @param $perms_def
 *  required -> the user must have all of these
 *  sufficient -> the user must have at least one of these
 * @return unknown_type
 */
function permmill_user_access_custom($perms_def) {
  if (is_array($perms_def['required'])) {
    foreach ($perms_def['required'] as $perm) {
      if (!user_access($perm)) {
        return FALSE; // A required does not match -> DENY
      }
    }
  }
  if (is_array($perms_def['sufficient'])) {
    foreach ($perms_def['sufficient'] as $perm) {
      if (user_access($perm)) {
        return TRUE; // A sufficient matches -> ALLOW
      }
    }
  }
  return FALSE; // No matching -> DENY
}


/**
 * CONST: Permission modes (operator)
 * @return array
 */
function _permmill_perm_types() {
  return array(
    PERMMILL_TYPE_REQUIRED => t("Required"),
    PERMMILL_TYPE_SUFFICIENT => t("Sufficient"),
  );
}


/**
 * CONST: Page name matching
 * @return array
 */
function _permmill_page_matches() {
  return array(
    PERMMILL_MATCH_EQUAL => t("Equal"),
    PERMMILL_MATCH_BEGINS => t("Begins with"),
    PERMMILL_MATCH_REGEXP => t("Regular expression"),
  );
}


/**
 * Get all the defined rules as array of objects
 * @return array
 */
function _permmill_get_all_rules() {
  $rows = array();
  $res = db_query("SELECT * FROM {permmill_pages}");
  while ($row = db_fetch_object($res)) {
    $rows[] = $row;
  }
  return $rows;
}

