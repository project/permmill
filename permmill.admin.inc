<?php

/*******************************************************************************
            ____                      __  ____ ____
           / __ \___  _________ ___  /  |/  (_) / /
          / /_/ / _ \/ ___/ __ `__ \/ /|_/ / / / / 
         / ____/  __/ /  / / / / / / /  / / / / /  
        /_/    \___/_/  /_/ /_/ /_/_/  /_/_/_/_/

  Provides your fine-grained Drupal permissions
  Author : Samuele Santi <s.santi AT squadrainformatica.com>
  Copyright (c) 2010 2V S.r.l.

*******************************************************************************/

/**
 * Admin overview page 
 * @return unknown_type
 */
function permmill_admin_settings_overview() {
  include_once drupal_get_path('module', 'system') .'/system.admin.inc';
  return system_admin_menu_block_page();
}

/**
 * List entries in the permmill_pages table
 * @return unknown_type
 */
function permmill_admin_menu_access_list() {
  $header = array(t("Id"), t("Match"), t("Match type"), t("Permission"), t("Perm type"), t("Actions"));
  $rows = array();
  
  $match_types = _permmill_page_matches();
  $perm_modes = _permmill_perm_types();
  
  $rules = _permmill_get_all_rules();
  foreach ($rules as $row) {
    $rows[] = array(
      l($row->id, 'admin/build/permmill/menu_access/'. $row->id .'/edit'),
      $row->page_match,
      $match_types[$row->match_type],
      $row->permission,
      $perm_modes[$row->perm_type],
      l(t("edit"), 'admin/build/permmill/menu_access/'. $row->id .'/edit'),
    );
  }
  return theme('table', $header, $rows);
}


/**
 * 
 * @param $form_state
 * @return unknown_type
 */
function permmill_admin_menu_access_form($form_state, $id = NULL) {
  $form = array();
  
  if ($id) {
    $res = db_query("SELECT * FROM {permmill_pages} WHERE id=%d", $id);
    $rule = db_fetch_object($res);
  }
  else {
    $rule = new stdClass();
    $rule->match_type = PERMMILL_MATCH_BEGINS;
    $rule->perm_type = PERMMILL_TYPE_SUFFICIENT;
  }
  
  $form['id'] = array(
    '#type' => 'hidden',
    "#value" => $id,
  );
  $form['page_match'] = array(
    '#type' => 'textfield',
    '#title' => t("Page Match"),
    '#default_value' => $rule->page_match,
    '#required' => TRUE,
    '#description' => t("Insert a string matching pages that would be affected by this rule."),
  );
  $form['match_type'] = array(
    '#type' => 'select',
    '#title' => t("Match type"),
    '#options' => _permmill_page_matches(),
    '#default_value' => $rule->match_type,
    '#required' => TRUE,
    '#description' => t("Choose the type of match that will be used to compare with menu path"),
  );
  $form['permission'] = array(
    '#type' => 'textfield',
    '#title' => t("Permission Name"),
    '#default_value' => $rule->permission,
    '#required' => TRUE,
    '#description' => t("Insert the permission that will be required/will give access to specified path."),
  );
  $form['perm_type'] = array(
    '#type' => 'select',
    '#title' => t("Permission rule type"),
    '#options' => _permmill_perm_types(),
    '#default_value' => $rule->perm_type,
    '#required' => TRUE,
    '#description' => t("Choose Required if the user MUST have this permission to access the selected page, Sufficient if the user has full access to this page if he as at least this permission."),
  );
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t("Save"),
  );
  if ($form_state['values']['id']) {
    $form['buttons']['delete'] = array(
      '#type' => 'submit',
      '#value' => t("Delete"),
      '#submit' => 'permmill_admin_menu_access_form_submit_delete',
    );
  }
  return $form;
}


/**
 * Save rule 
 * @param $form
 * @param $form_state
 * @return unknown_type
 */
function permmill_admin_menu_access_form_submit($form, &$form_state) {
  //dpm($form_state['values']);
  $form_values = $form_state['values'];
  $row = array(
    'page_match' => $form_values['page_match'],
    'match_type' => $form_values['match_type'],
    'permission' => $form_values['permission'],
    'perm_type' => $form_values['perm_type'],
  );
  
  if ($form_values['id']) {
    $row['id'] = $form_values['id'];
    drupal_write_record('permmill_pages', $row, array('id'));
  }
  else {
    drupal_write_record('permmill_pages', $row);
  }
  
  $match_types = _permmill_page_matches();
  $perm_modes = _permmill_perm_types();
  drupal_set_message(t("Permission rule !id: %permission %perm_type for page %page_match (%match_type) saved.", array(
    '!id' => $row['id'],
    '%page_match' => $row['page_match'],
    '%match_type' => $match_types[$row['match_type']],
    '%permission' => $row['permission'],
    '%perm_type' => $perm_modes[$row['perm_type']],
  )));
  drupal_set_message(t("Please make sure to !link_rebuild in order to apply new rules!", array(
    '!link_rebuild' => module_exists('devel') ? l(t('rebuild menus'), 'devel/menu/reset') : t('rebuild menus'),
  )));
  
  $form_state['redirect'] = 'admin/build/permmill/menu_access';
}


/**
 * Delete a rule
 * @param $form
 * @param $form_state
 * @return unknown_type
 */
function permmill_admin_menu_access_form_submit_delete($form, &$form_state) {
  drupal_set_message(t("Will delete permission rule %id", array('%id' => $form_state['values']['id'])));
}

