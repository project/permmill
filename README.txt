********************************************************************************
            ____                      __  ____ ____
           / __ \___  _________ ___  /  |/  (_) / /
          / /_/ / _ \/ ___/ __ `__ \/ /|_/ / / / / 
         / ____/  __/ /  / / / / / / /  / / / / /  
        /_/    \___/_/  /_/ /_/ /_/_/  /_/_/_/_/

  Provides your fine-grained Drupal permissions
  Author : Samuele Santi <s.santi AT squadrainformatica.com>
  Copyright (c) 2010 2V S.r.l.

********************************************************************************

Module for use in Stock Websites.
It adds some more permissions on the administration panel to let you configure
more fine-grained access rules to administrative pages.
